import { ImoveisRuraisComponent } from './components/imoveis-rurais/imoveis-rurais.component';
import { ImoveisUrbanosComponent } from './components/imoveis-urbanos/imoveis-urbanos.component';
import { CadImovelComponent } from './components/cad-imovel/cad-imovel.component';
import { HomeComponent } from './components/home/home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
{path:"",component:HomeComponent},
{path:"addImovel",component:CadImovelComponent},
{path:"rurais",component:ImoveisRuraisComponent},
{path:"urbanos",component:ImoveisUrbanosComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
