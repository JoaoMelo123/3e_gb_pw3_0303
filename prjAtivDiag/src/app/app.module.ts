import { HomeModule } from './components/home/home.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ImoveisRuraisModule } from './components/imoveis-rurais/imoveis-rurais.module';
import { ImoveisUrbanosModule } from './components/imoveis-urbanos/imoveis-urbanos.module';
import { CadImovelModule } from './components/cad-imovel/cad-imovel.module';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HomeModule,
    ImoveisRuraisModule,
    ImoveisUrbanosModule,
    CadImovelModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
