import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DestaquesComponent } from './destaques/destaques.component';
import { ImoveisRuraisComponent } from './imoveis-rurais.component';



@NgModule({
  declarations: [
    DestaquesComponent,
    ImoveisRuraisComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ImoveisRuraisModule { }
