import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { TopoComponent } from './topo/topo.component';
import { DestaquesComponent } from './destaques/destaques.component';

import { OptionComponent } from './option/option.component';
import { AnfitriaoComponent } from './anfitriao/anfitriao.component';
import { FooterComponent } from './footer/footer.component';



@NgModule({
  declarations: [
    TopoComponent,
    DestaquesComponent,

    HomeComponent,
    OptionComponent,
    AnfitriaoComponent,
    FooterComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    HomeComponent
  ]
})
export class HomeModule { }
